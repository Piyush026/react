(function ($) {
  "use strict";

 /*  Preloader */
$(window).on('load',function(){
  var preLoder = $(".preloader");
  preLoder.fadeOut(1000);
   
}); 
/*  Fixed Header*/
 $(window).scroll(function() {
        if ($(this).scrollTop() > 90) {
            $(".header-area").addClass("sticky");
        } else {
            $(".header-area").removeClass("sticky");
        }
});
 $('.feature-slider').slick({
  slidesToShow: 4,
  arrows: true,
  dots: false,
  responsive: [ 
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3
      }
     },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 567,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
/*  Mean Menu */
  $('#navbar-menu').meanmenu({
    meanScreenWidth: "991",
    meanMenuContainer: '.header-navbar',  
    meanMenuOpen: "<span></span><span></span><span></span>",
    meanMenuClose: "<span></span><span></span><span></span>",
    siteLogo: "",
  });
/*  Aos  */ 
AOS.init({
  offset: 120,
  delay: 10, 
  duration: 1000,
  easing: 'ease', 
  once: true, 
  mirror: false, 
  anchorPlacement: 'top-bottom', 
}); 

/*  Select Option  */
$('.select-option').select2({
    "id": "ami-id",
});
/*  Scrolltop  */
  function scrolltop(){
      
      var wind = $(window);
  
      wind.on("scroll" ,function(){
  
          var scrollTop = wind.scrollTop();
  
          if(scrollTop >=500) {
  
              $(".footer-back").addClass("show");
  
          } else {
          
              $(".footer-back").removeClass("show");
          }
          
  });

  $(".footer-back").on("click", function(){

      var bodyTop =  $("html, body");
 
        bodyTop.animate({scrollTop: 0},500,"easeOutCubic");
  });
 
  }
  
 scrolltop();

  /*  Range Slider 1  */
     if($("#slider-snap").length > 0){
var snapSlider = document.getElementById('slider-snap');
noUiSlider.create(snapSlider, {
    start: [0, 100000], 
    connect: true,
    range: {
        'min': [0],
        'max': [100000]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min').innerHTML = values[0];
    document.getElementById('range-max').innerHTML = values[1];
  });
}
  /*  Range Slider 2  */
       if($("#slider-snap2").length > 0){
var snapSlider = document.getElementById('slider-snap2');
noUiSlider.create(snapSlider, {
    start: [0, 100000], 
    connect: true,
    range: {
        'min': [0],
        'max': [100000]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min2').innerHTML = values[0];
    document.getElementById('range-max2').innerHTML = values[1];
  });
}
  /*  Range Slider 3  */
       if($("#slider-snap3").length > 0){
var snapSlider = document.getElementById('slider-snap3');
noUiSlider.create(snapSlider, {
    start: [0, 100000], 
    connect: true,
    range: {
        'min': [0],
        'max': [100000]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min3').innerHTML = values[0];
    document.getElementById('range-max3').innerHTML = values[1];
  });
}
  /*  Range Slider 4  */
       if($("#slider-snap4").length > 0){
var snapSlider = document.getElementById('slider-snap4');
noUiSlider.create(snapSlider, {
    start: [0, 100000], 
    connect: true,
    range: {
        'min': [0],
        'max': [100000]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min4').innerHTML = values[0];
    document.getElementById('range-max4').innerHTML = values[1];
  });
}
  /*  Range Slider 5  */
       if($("#slider-snap5").length > 0){
var snapSlider = document.getElementById('slider-snap5');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },
   format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min5').innerHTML = values[-0];
    document.getElementById('range-max5').innerHTML = values[1];
  });
}
  /*  Range Slider 6  */
       if($("#slider-snap6").length > 0){
var snapSlider = document.getElementById('slider-snap6');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },   
    format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min6').innerHTML = values[-0];
    document.getElementById('range-max6').innerHTML = values[1];
  });
}
  /*  Range Slider 7  */
       if($("#slider-snap7").length > 0){
var snapSlider = document.getElementById('slider-snap7');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min7').innerHTML = values[-0];
    document.getElementById('range-max7').innerHTML = values[1];
  });
}
  /*  Range Slider 8  */
       if($("#slider-snap8").length > 0){
var snapSlider = document.getElementById('slider-snap8');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },
   format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min8').innerHTML = values[-0];
    document.getElementById('range-max8').innerHTML = values[1];
  });
}
  /*  Range Slider 9  */
       if($("#slider-snap9").length > 0){
var snapSlider = document.getElementById('slider-snap9');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },   
    format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min9').innerHTML = values[-0];
    document.getElementById('range-max9').innerHTML = values[1];
  });
}
  /*  Range Slider 10  */
       if($("#slider-snap10").length > 0){
var snapSlider = document.getElementById('slider-snap10');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min10').innerHTML = values[-0];
    document.getElementById('range-max10').innerHTML = values[1];
  });
}
  /*  Range Slider 11  */
       if($("#slider-snap11").length > 0){
var snapSlider = document.getElementById('slider-snap11');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min11').innerHTML = values[-0];
    document.getElementById('range-max11').innerHTML = values[1];
  });
}
  /*  Range Slider 12  */
       if($("#slider-snap12").length > 0){
var snapSlider = document.getElementById('slider-snap12');
noUiSlider.create(snapSlider, {
    start: [-100, 100], 
    connect: true,
    range: {
        'min': [-100],
        'max': [100]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min12').innerHTML = values[-0];
    document.getElementById('range-max12').innerHTML = values[1];
  }); 
}

  /*  Range Slider 13  */
       if($("#slider-snap13").length > 0){
var snapSlider = document.getElementById('slider-snap13');
noUiSlider.create(snapSlider, {
    start: [0, 365], 
    connect: true,
    range: {
        'min': [0],
        'max': [365]
    },
       format: {
        to: (v) => parseFloat(v).toFixed(0),
        from: (v) => parseFloat(v).toFixed(0)
    }
});
  snapSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('range-min13').innerHTML = values[-0];
    document.getElementById('range-max13').innerHTML = values[1];
  }); 
}




}(jQuery));