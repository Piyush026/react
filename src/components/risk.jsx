import React,{ useState, useEffect } from "react";
import Charges from "./Charges";
import Pagination from "./Pagination";
import '../css/style.css';

// function roundof(number) {
//   return Math.round(number * 10) / 10;
// }

// function formatDate(stringDate){
//   var date=new Date(stringDate);
//   return date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear();
// }

const  Risk = ({ match }) =>{
  const {
    params: { personId },
  } = match;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [currentPage,setCurrentPage] = useState(1);
  const [postsPerPage] = useState(5);

  useEffect(() => {
    fetch(`http://127.0.0.1:8000/risk/${personId}`, {})
      .then((res) => res.json())
      .then((response) => {
        setData(response);
        setIsLoading(false);
        console.log(`http://127.0.0.1:8000/risk/${personId}`);
      })
      .catch((error) => console.log(error));
  }, [personId]);

  const indexOfLastPost = currentPage*postsPerPage;
  const indexOfFirstPost = indexOfLastPost-postsPerPage;
  const currentPostt = data.slice(indexOfFirstPost,indexOfLastPost);
//change page function
const paginate = (pageNumber) => setCurrentPage(pageNumber);
console.log(currentPage);
  
// const handleClick = (event) => {
//   setCurrentPage(Number(event.target.id));
// };

return(
    
    <>
    { !isLoading && (
      
      <Charges data={currentPostt} loading={isLoading}/>
    )}
    
    <ul className="pagination">
    {currentPage > 1 && <a onClick={() => setCurrentPage(currentPage - 1)} href="#" className="page-link">{'<'}</a>}
          <Pagination postsPerPage={postsPerPage} totalPosts={data.length} paginate={paginate}/>
          {currentPage < postsPerPage -2 && <a onClick={() => setCurrentPage(currentPage + 1)} href="#" className="page-link">{'>'}</a>}
         
    </ul>
   
    </>
  )
}



export default Risk;
