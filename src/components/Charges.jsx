import React from 'react';

const roundof=(number)=> {
  return Math.round(number * 10) / 10;
}

function formatDate(stringDate){
  var date=new Date(stringDate);
  return date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear();
}

const Charges = ({data,loading}) => {
  if (loading){
    return <h1>Loading,,</h1>
  }
    return (
        
            <>
      <div class="main-content-area pb-6">
    <div class="main-wrapper">
      <div class="container-fluid">
            {/* <ul class="navbar-bottom-menu navbar-nav mb-4">
              <li class="nav-item">
                <a class="nav-link" href="#">Bank Loans</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Credit Bureau</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Litigation Cases</a>
              </li>
            </ul> */}
            <div class="bg-light rounded p-4 mb-4">
          <h5 class="mb-3"><strong>Open Charges / Borrowings</strong></h5>
          <div class="table-responsive">
            <table class="table m-0">
              <thead>
                <tr>
                  <th scope="col">Charge Holder</th>
                  <th scope="col">Amount</th>
                  <th scope="col">Outstanding years</th>
                  <th scope="col">Creation Date</th>
                  <th scope="col">Last Modification Date</th>
                  
                </tr>
              </thead>
              <tbody>
              {data.map((person, index) => (
                <tr key={index}>
                  <td class="max-table-width">
                    <span class="fw-bold pe-md-4">
                      {person.chargeholdername}</span></td>
                  <td>{roundof(person.amount)}</td>
                  <td>0.3 Years</td>
                  <td>{formatDate(person.chrcreationdate)}</td>
                  <td>{person.chrmodificatondate}</td>
                  
                </tr> 
                ))}
              </tbody>
            </table>
          </div>
        </div>
        </div>
    </div>
    </div>
    </>
          
    )
}
export default Charges;