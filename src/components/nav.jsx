import React from "react";
import brandLogo from "../images/header-brand.png";
import searchLogo from "../images/search.png";
import heartLogo from "../images/heart.png";
import alertLogo from "../images/alert.png";
import authorLogo from "../images/author-image.png";
import '../css/style.css'
import { NavLink } from "react-router-dom";


const Nav  =  () => {
    return(
        <>
        <nav className="header-navbar navbar navbar-expand-xl bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="index.html"><img src={brandLogo} alt="header-brand"/></a>
        
        <button className="navbar-toggler control-toggler ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent">
          <span className="toggler-icon"></span>
          <span className="toggler-icon"></span>
          <span className="toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-main-menu navbar-nav me-auto">
            <li className="nav-item">
              <a className="nav-link active" href="#">Overview</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Skateholders</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Financial</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Web & Social</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Comparables</a>
            </li>
           <li className="nav-item">
              <a className="nav-link" href="#">Compliance</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Team</a>
            </li> 
            <li className="nav-item">
              <a className="nav-link" href="#">Comparables</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">News</a>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/charges/:personId">Risk</NavLink>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Report</a>
            </li>
          </ul>
        </div>
        <ul className="navbar-nav-profile navbar-nav align-items-center ms-auto">
          <li className="nav-item me-md-4 me-3">
            <a className="search-icon nav-link" href="search.html"><img src={searchLogo} alt="search here"/></a>
          </li>
          <li className="nav-item me-md-4 me-3">
            <a className="heart-icon nav-link" href="#"><img src={heartLogo} alt="alert here"/></a>
          </li>
          <li className="nav-item me-md-4 me-3">
            <a className="alert-icon nav-link" href="#"><img src={alertLogo} alt="alert here"/></a>
          </li>
          <li className="nav-item">
            <a className="profile-avata nav-link" href="#"><img src={authorLogo} alt="search here"/></a>
          </li>
        </ul>
      </div>
    </nav>
        </>
    );
}

export default Nav;