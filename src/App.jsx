import React from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Nav from "./components/nav";
import Risk from "./components/risk";
import {Switch,Route} from 'react-router-dom';
const  App = () =>{
  return(
    <>
    <Nav/>
    <Switch>
      <Route exact path = "/" component = {Home}/>
      <Route exact path = "/about" component = {About}/>
      <Route exact path = "/contact" component = {Contact}/>
      <Route exact path = "/charges/:personId" component = {Risk}/>
    </Switch>
      
    </>
  )
  
    // const [count, setCount] = React.useState(0);
    
    // const components = [
    //     <div>1</div>,
    //     <div>2</div>,
    //     <div>3</div>,
    //     <div>4</div>
    // ]

    // return <div>
    //     {
    //         // render component from our components array
    //         components[count]
    //     }

    //     {/* show previous button if we are not on first element */}
    //     {count > 0 && <button onClick={() => setCount(count - 1)}>prev</button>}

    //     {/* hide next button if we are at the last element */}
    //     {count < components.length - 1 && <button onClick={() => setCount(count + 1)}>next</button>}
    // </div>
}




export default App;
